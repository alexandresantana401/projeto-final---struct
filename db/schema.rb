# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_16_185939) do

  create_table "comments", force: :cascade do |t|
    t.string "comentario"
    t.integer "user_id"
    t.integer "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_comments_on_task_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "validade"
  end

  create_table "levels", force: :cascade do |t|
    t.integer "nivel_max"
    t.integer "exp_base"
    t.float "porcent_xp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "members", force: :cascade do |t|
    t.integer "user_id"
    t.string "lider"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.integer "group_id"
    t.index ["group_id"], name: "index_members_on_group_id"
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "people_tasks", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "group_id"
    t.integer "task_id"
    t.index ["group_id"], name: "index_people_tasks_on_group_id"
    t.index ["task_id"], name: "index_people_tasks_on_task_id"
    t.index ["user_id"], name: "index_people_tasks_on_user_id"
  end

  create_table "shop_items", force: :cascade do |t|
    t.string "nome"
    t.integer "preco"
    t.integer "nivel_minimo"
    t.boolean "consumivel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skills", force: :cascade do |t|
    t.string "nome"
    t.integer "dano"
    t.integer "defesa"
    t.integer "nivel_maximo"
    t.integer "nivel_requerido"
    t.integer "multiplicador_dano"
    t.integer "multiplicador_defesa"
    t.integer "niveis_upar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_class_id"
    t.integer "user_subclass_id"
    t.index ["user_class_id"], name: "index_skills_on_user_class_id"
    t.index ["user_subclass_id"], name: "index_skills_on_user_subclass_id"
  end

  create_table "task_classes", force: :cascade do |t|
    t.integer "quantidade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_class_id"
    t.index ["user_class_id"], name: "index_task_classes_on_user_class_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "titulo"
    t.text "descricao"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "task_class_id"
    t.integer "lider_id"
    t.integer "aumento_xp", default: 0
    t.integer "aumento_prazo", default: 0
    t.float "xp_base"
    t.float "moedas_base"
    t.float "moedas_atual"
    t.float "xp_atual"
    t.datetime "prazo_atual"
    t.datetime "prazo_base"
    t.index ["task_class_id"], name: "index_tasks_on_task_class_id"
  end

  create_table "user_classes", force: :cascade do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_skills", force: :cascade do |t|
    t.integer "user_id"
    t.integer "skill_id"
    t.integer "nivel_habilidade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["skill_id"], name: "index_user_skills_on_skill_id"
    t.index ["user_id"], name: "index_user_skills_on_user_id"
  end

  create_table "user_subclasses", force: :cascade do |t|
    t.string "nome"
    t.integer "user_class_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_class_id"], name: "index_user_subclasses_on_user_class_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nome"
    t.string "apelido"
    t.string "email"
    t.date "data_nascimento"
    t.string "local_nascimento"
    t.string "genero"
    t.integer "level"
    t.integer "qnt_moedas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "crypted_password"
    t.string "salt"
    t.integer "user_class_id"
    t.index ["user_class_id"], name: "index_users_on_user_class_id"
  end

end
