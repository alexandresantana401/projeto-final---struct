class CreateHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :houses do |t|
      t.string :nome
      t.string :members
      t.date :validade

      t.timestamps
    end
  end
end
