class AddCollumUserClassToTaskClass < ActiveRecord::Migration[5.2]
  def change
    add_reference :task_classes, :user_class, foreign_key: true
  end
end
