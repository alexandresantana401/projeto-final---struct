class ChangeCollumnLiderFromMembers < ActiveRecord::Migration[5.2]
  def change
    change_column :members, :lider, :string
  end
end
