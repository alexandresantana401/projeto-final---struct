class AddColumnLiderIdToTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :lider_id, :integer
  end
end
