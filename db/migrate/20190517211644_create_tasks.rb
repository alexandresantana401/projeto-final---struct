class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :titulo
      t.text :descricao
      t.references :user, foreign_key: true
      t.integer :pontuacao
      t.integer :moedas
      t.string :status
      t.datetime :prazo

      t.timestamps
    end
  end
end
