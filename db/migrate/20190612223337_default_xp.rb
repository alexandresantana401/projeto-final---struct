class DefaultXp < ActiveRecord::Migration[5.2]
  def self.up
    change_column :tasks, :aumento_xp, :integer, :default => 0
  end
end
