class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nome
      t.string :apelido
      t.string :email
      t.date :data_nascimento
      t.string :local_nascimento
      t.string :genero
      t.integer :level
      t.string :classe_jogador
      t.integer :qnt_moedas
      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
