class ChangeCollumStatusFromMembers < ActiveRecord::Migration[5.2]
  def change
    change_column :members, :status, :string
  end
end
