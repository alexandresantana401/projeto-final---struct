class RemoveNivel < ActiveRecord::Migration[5.2]
  def up
    drop_table :nivels
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
