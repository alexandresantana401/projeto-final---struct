class ChangeXpType < ActiveRecord::Migration[5.2]
  def change
    change_column :tasks, :xp_atual, :float
  end
end
