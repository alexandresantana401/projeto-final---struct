class CreateTaskClasses < ActiveRecord::Migration[5.2]
  def change
    create_table :task_classes do |t|
      t.references :group, foreign_key: true
      t.integer :quantidade

      t.timestamps
    end
  end
end
