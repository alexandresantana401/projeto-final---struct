class RemoveMembersFromHouses < ActiveRecord::Migration[5.2]
  def change
    remove_column :houses, :members, :string
  end
end
