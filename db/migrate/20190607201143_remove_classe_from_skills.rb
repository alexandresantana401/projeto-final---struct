class RemoveClasseFromSkills < ActiveRecord::Migration[5.2]
  def change
    remove_column :skills, :classe, :string
  end
end
