class ChangeXpFromTasks < ActiveRecord::Migration[5.2]
  def change
    change_column :tasks, :xp_base, :float
  end
end
