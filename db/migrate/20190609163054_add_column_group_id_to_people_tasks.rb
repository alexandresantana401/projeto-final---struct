class AddColumnGroupIdToPeopleTasks < ActiveRecord::Migration[5.2]
  def change
    add_reference :people_tasks, :group_id, foreign_key: true
  end
end
