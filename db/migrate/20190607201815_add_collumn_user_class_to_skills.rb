class AddCollumnUserClassToSkills < ActiveRecord::Migration[5.2]
  def change
    add_reference :skills, :user_class, foreign_key: true
  end
end
