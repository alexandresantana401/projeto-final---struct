class CreateNivels < ActiveRecord::Migration[5.2]
  def change
    create_table :nivels do |t|
      t.integer :nivel_max
      t.integer :exp_base
      t.float :porcent_xp

      t.timestamps
    end
  end
end
