class CreateUserSubclasses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_subclasses do |t|
      t.string :nome
      t.references :user_class, foreign_key: true

      t.timestamps
    end
  end
end
