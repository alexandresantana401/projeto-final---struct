class CreateSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :skills do |t|
      t.string :nome
      t.integer :dano
      t.integer :defesa
      t.integer :nivel_maximo
      t.integer :nivel_requerido
      t.integer :multiplicador_dano
      t.integer :multiplicador_defesa
      t.integer :niveis_upar
      t.string :classe
      t.string :subclasse

      t.timestamps
    end
  end
end
