class RemoveColumnHouseFromPeopleTasks < ActiveRecord::Migration[5.2]
  def change
    remove_reference :people_tasks, :house, foreign_key: true
  end
end
