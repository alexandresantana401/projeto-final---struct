class RemoveColumnPontuacaoFromTasks < ActiveRecord::Migration[5.2]
  def change
    remove_column :tasks, :pontuacao, :integer
  end
end
