class RemoveColumnMoedasFromTasks < ActiveRecord::Migration[5.2]
  def change
    remove_column :tasks, :moedas, :integer
  end
end
