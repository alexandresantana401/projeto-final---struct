class RemoveColumnGroupIdFromTaskClasses < ActiveRecord::Migration[5.2]
  def change
    remove_column :task_classes, :group_id, :integer
  end
end
