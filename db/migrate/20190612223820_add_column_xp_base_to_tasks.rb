class AddColumnXpBaseToTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :xp_base, :integer
  end
end
