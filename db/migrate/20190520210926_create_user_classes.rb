class CreateUserClasses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_classes do |t|
      t.string :nome

      t.timestamps
    end
  end
end
