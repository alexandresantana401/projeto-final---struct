class AddColumnTaskToPeopleTasks < ActiveRecord::Migration[5.2]
  def change
    add_reference :people_tasks, :task, foreign_key: true
  end
end
