class AddColumnGroupToPeopleTasks < ActiveRecord::Migration[5.2]
  def change
    add_reference :people_tasks, :group, foreign_key: true
  end
end
