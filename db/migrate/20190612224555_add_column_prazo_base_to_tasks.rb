class AddColumnPrazoBaseToTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :prazo_base, :datetime
  end
end
