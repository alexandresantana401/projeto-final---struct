class RemoveColumnSubclasseFromSkills < ActiveRecord::Migration[5.2]
  def change
    remove_column :skills, :subclasse, :string
  end
end
