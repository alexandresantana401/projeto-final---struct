class RemoveColumnValidadeFromGroups < ActiveRecord::Migration[5.2]
  def change
    remove_column :groups, :validade, :date
  end
end
