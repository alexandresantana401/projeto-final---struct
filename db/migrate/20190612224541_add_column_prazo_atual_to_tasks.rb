class AddColumnPrazoAtualToTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :prazo_atual, :datetime
  end
end
