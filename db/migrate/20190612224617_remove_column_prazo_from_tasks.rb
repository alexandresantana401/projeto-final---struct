class RemoveColumnPrazoFromTasks < ActiveRecord::Migration[5.2]
  def change
    remove_column :tasks, :prazo, :datetime
  end
end
