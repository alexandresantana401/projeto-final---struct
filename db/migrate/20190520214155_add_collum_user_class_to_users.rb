class AddCollumUserClassToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :user_class, foreign_key: true
  end
end
