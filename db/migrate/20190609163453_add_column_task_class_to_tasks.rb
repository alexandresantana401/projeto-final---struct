class AddColumnTaskClassToTasks < ActiveRecord::Migration[5.2]
  def change
    add_reference :tasks, :task_class, foreign_key: true
  end
end
