class AddMemberToHouses < ActiveRecord::Migration[5.2]
  def change
    add_reference :houses, :member, foreign_key: true
  end
end
