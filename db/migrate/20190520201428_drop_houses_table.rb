class DropHousesTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :houses
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
