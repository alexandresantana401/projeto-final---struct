class AddCollumnUserSubclassToSkills < ActiveRecord::Migration[5.2]
  def change
    add_reference :skills, :user_subclass, foreign_key: true
  end
end
