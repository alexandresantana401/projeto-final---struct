class CreateSubgroups < ActiveRecord::Migration[5.2]
  def change
    create_table :subgroups do |t|
      t.string :nome
      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
