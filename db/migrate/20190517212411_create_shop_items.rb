class CreateShopItems < ActiveRecord::Migration[5.2]
  def change
    create_table :shop_items do |t|
      t.string :nome
      t.integer :preco
      t.integer :nivel_minimo
      t.boolean :consumivel

      t.timestamps
    end
  end
end
