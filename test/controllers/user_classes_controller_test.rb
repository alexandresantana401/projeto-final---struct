require 'test_helper'

class UserClassesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get user_classes_new_url
    assert_response :success
  end

  test "should get new_subclass" do
    get user_classes_new_subclass_url
    assert_response :success
  end

  test "should get edit_subclass" do
    get user_classes_edit_subclass_url
    assert_response :success
  end

  test "should get edit" do
    get user_classes_edit_url
    assert_response :success
  end

  test "should get index" do
    get user_classes_index_url
    assert_response :success
  end

  test "should get show" do
    get user_classes_show_url
    assert_response :success
  end

end
