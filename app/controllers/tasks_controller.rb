class TasksController < ApplicationController
    require 'date'
    def index
        #@tasks = Task.order :id
        @tasks = Task.all
    end
    
    def new
        #New e create para criarmos uma tarefa (Não precisamos selecionar usuários aqui ainda)
        @task = Task.new()
    end

    def create
        #New e create para criarmos uma tarefa (Não precisamos selecionar usuários aqui ainda)
        @task = Task.new(task_params)
        @task.status = "incompleto"
        @task.lider_id = current_user.id
        @task.prazo_atual = @task.prazo_base
        @task.xp_atual = @task.xp_base
        @task.moedas_atual = @task.moedas_base

        if @task.save
            redirect_to task_path(@task)
        else
            render 'new'
        end
    end

    def edit
        #Edit e update para alterarmos uma tarefa. (Aqui só podemos editar o título e a descrição)
        @task = Task.find(params[:id])
    end
    
    def update
        #Edit e update para alterarmos uma tarefa. (Aqui só podemos editar o título e a descrição)
        @task = Task.find(params[:id])
        if @task.update(task_params)
            redirect_to task_path(@task)
        else
            render 'edit'
        end        
    end

    def destroy
        #Destroy para apagarmos uma tarefa (cuidado aqui, apenas o usuário que criou a tarefa pode apagá-la)  
        @task = Task.find(params[:id])
        @people_tasks = PeopleTask.where(task_id: params[:id])
        @comments = Comment.where(task_id: params[:id])

        @comments.destroy_all
        @people_tasks.destroy_all
        @task.destroy
        redirect_to tasks_path
    end

    def show
        #aqui poderemos visualizar os dados da tarefa e teremos um botão de finalizar a tarefa, visível apenas para o criador da tarefa.
        @task = Task.find(params[:id])
        @users = User.all
        @groups = Group.all
        @people_tasks = PeopleTask.where(task_id: params[:id])

        #testar se ja passou metade do prazo
        @time = Time.current
        @time_start = @time - @task.created_at
        @time_end = @task.prazo_atual - @time
        if @time_start < @time_end
            @prazo = true
        else
            @prazo = false
        end
    end

    def show_comments 
        @comments = Comment.where(task_id: params[:id])
    end

    def assign_new
        @participant = PeopleTask.new()
        @task = Task.find(params[:id])
    end

    def assign
        #Vamos precisar de um método para adicionar um usuário ou um grupo à tarefa. Lembrem-se que criou a tarefa não está participando da tarefa, se ele for participar também deve ser adicionado.
        if params[:tipo_usuario] == 'user'
            @participant = PeopleTask.new(user_id: current_user.id, task_id: params[:id])
            if @participant.save
                redirect_to tasks_path
            else
                render '#assign_new'
            end
        else
            if User.find(current_user.id).member && User.find(current_user.id).member.lider == 'SIM'
                @participant = PeopleTask.new(group_id: current_user.member.group_id, task_id: params[:id])
                if @participant.save
                    redirect_to tasks_path
                else
                    render '#assign_new'
                end
            end
        end
    end

    def finish
        #vamos precisar também de um método que permita finalizar a tarefa.
        #Quando a tarefa for finalizada as moedas e xp devem ser distribuídos
        #igualmente entre os participantes, notem que se tiver um usuário e um 
        #grupo participante, a distribuição é feita 50% para o usuário e 50% para 
        #o grupo, depois os 50% que ficou para o grupo é distribuído igualmente 
        #entre os membros do grupo. No momento da distribuição deve ser verificado 
        #se um usuário tem xp suficiente para passar de nível.'
        #'Obs: Se a tarefa for finalizada fora do prazo fina, deve ter uma penalidade de 90% de xp e 100% de moedas.
    end

    def increase_xp        
        @task = Task.find(params[:id])
        @porcentagem = @task.aumento_xp + params[:xp].to_f
        @aumento_valor = @task.xp_atual + @task.xp_atual * params[:xp].to_f/100
        @task.update(aumento_xp: @porcentagem, xp_atual: @aumento_valor)
        redirect_to task_path(@task)
    end

    def increase_time
        @task = Task.find(params[:id])
        @aumento_porcentagem = params[:prazo].to_f
        @porcentagem = @task.aumento_prazo + @aumento_porcentagem
        @aumento_valor = @task.prazo_atual.to_f + (@task.prazo_base.to_f - @task.created_at.to_f)* params[:prazo].to_f/100

        case @aumento_porcentagem
            when 10
                @moedas = @task.moedas_atual*0.55 #penalidade de 45%
                @xp = @task.xp_atual*0.55
            when 20
                @moedas = @task.moedas_atual*0.4 #penalidade de 60%
                @xp = @task.xp_atual*0.4
            when 30
                @moedas = @task.moedas_atual*0.25 #penalidade de 75%
                @xp = @task.xp_atual*0.25
        end

        @task.update(aumento_prazo: @porcentagem, prazo_atual: Time.at(@aumento_valor), xp_atual:@xp, moedas_atual:@moedas)
        redirect_to task_path(@task)
    end

    private
      def task_params
        params.require(:task).permit(:titulo, :descricao, :user_id, :xp_base, :moedas_base, :status, :prazo_base, :created_at, :updated_at,:grupo)
      end
end
