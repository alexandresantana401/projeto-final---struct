class SkillsController < ApplicationController
    
    def index
        redirect_to user_classes_path
    end

    def new
        @skill = Skill.new()
        @userclass = UserClass.all.collect{|userclass|[userclass.nome, userclass.id]}
        @subclass = UserSubclass.all.collect{|usersubclass|[usersubclass.nome, usersubclass.id]}
    end

    def create
        @skill = Skill.new(skill_params)
        @userclass = UserClass.all.collect{|userclass|[userclass.nome, userclass.id]}
        @subclass = UserSubclass.all.collect{|usersubclass|[usersubclass.nome, usersubclass.id]}
        
        if @skill.save
           redirect_to user_classes_path
        else
            render 'new'
        end
    end

    def edit
        @skill = Skill.find(params[:id])
    end

    def update
        @skill = Skill.find(params[:id])
        if @skill.update(skill_params)
              redirect_to user_classes_path
        else
              render 'edit'
        end
    end
    
    def destroy
        @skill = Skill.find(params[:id])
        @skill.destroy
        redirect_to user_classes_path
    end


    private
    def skill_params
        params.require(:skill).permit(:nome, :dano, :defesa, :nivel_maximo, :nivel_requerido, :multiplicador_dano, :multiplicador_defesa, :niveis_upar, :user_class_id, :user_subclass_id)
    end



end
