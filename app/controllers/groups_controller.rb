class GroupsController < ApplicationController
  def new
    @group = Group.new()
    @member = Member.new()
  end

  def create
    @group = Group.new(group_params)
    @member = Member.new()
    @member.user_id = current_user.id
    @member.lider = 'SIM'
    @member.status = 'ATIVO'
    if @group.save
      @member.group_id = @group.id
      if @member.save
        redirect_to group_path(@group)
      end
    else
      render 'new'
    end
  end

  def index
    @groups = Group.all
  end

  def show
    @group = Group.find(params[:id])
    if current_user and current_user.member
      @member = User.find(current_user.id).member
    end
  end

  def edit
    @group = Group.find(params[:id])
    
  end

  def update
    @group = Group.find(params[:id])
    if @group.update(group_params)
      redirect_to groups_path
    else
      render 'edit'
    end
  end

  def destroy
    @group = Group.find(params[:id])
    @group.members.each do |member|
      member.destroy
    end
    @group.destroy
    redirect_to groups_path
  end

  def new_member
    @member = Member.new()
    @users = User.all.collect{|user|[user.nome, user.id]}
  end

  def create_member
    @member = Member.new(user_id:params[:user_id], lider:params[:lider], group_id: params[:id])
    @users = User.all.collect{|user|[user.nome, user.id]}
    @member.status = "PENDENTE"
    if @member.save
      redirect_to group_path(@member.group)
    else
      render 'new_member'
    end
  end

  def edit_member
    @member = Member.find(params[:id])
  end

  def update_member
    @member = Member.find(params[:id])
    @member.update(status:params[:button])
    redirect_to user_path(@member.user)
  end

  def destroy_member
    @member = Member.find(params[:id])
    @group = @member.group
    @member.destroy
    redirect_to group_path(@group)
  end

  private
    def group_params
      params.require(:group).permit(:nome, :validade)
    end

    def member_params
      params.permit(:user_id, :lider, :id)
    end
end

