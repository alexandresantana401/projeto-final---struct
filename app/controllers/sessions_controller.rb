class SessionsController < ApplicationController

    def new
        if current_user
            redirect_to users_path
        else
            @user = User.new()
        end
    end

    def create
        if login(params[:user][:email],params[:user][:password])
            redirect_back_or_to root_url
        else
            render 'new'
        end

    end

    def destroy
        logout
        redirect_back(fallback_location: root_path)
    end

end
