class CommentsController < ApplicationController
    def new
        @comment = Comment.new()
        @user = User.all.collect{|user|[user.nome, user.id]}
        @task = Task.all.collect{|task|[task.titulo, task.id]}
    end

    def create
        @user = User.all.collect{|user|[user.nome, user.id]}
        @task = Task.all.collect{|task|[task.titulo, task.id]}
        @comment = Comment.new(comment_params)
        if @comment.save
            redirect_to tasks_path
        else
            render 'new'
        end
    end

    def update
        @comment = Comment.find(params[:id])
        if @comment.update(comment_params)
          redirect_to tasks_path
        else
          render 'edit'
        end
      end
    
      def edit
        @comment = Comment.find(params[:id])
      end

      def destroy
        @comment = Comment.find(params[:id])
        @comment.destroy

        redirect_to tasks_path
      end

      private
      def comment_params
        params.require(:comment).permit(:user_id, :task_id, :comentario)
      end

end
