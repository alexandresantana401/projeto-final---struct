class UsersController < ApplicationController
  def index
    @users = User.order :id
  end

  def show
    @user = User.find(params[:id])
    if @user.member
      if @user.member.status == "ATIVO"
        @status = @user.member.group.nome
      else
        @status = "Não está em um grupo"
      end
    else
      @status = "Não está em um grupo"
    end
  end

  def new
    @user = User.new()
    @userclass = UserClass.all.collect{|userclass|[userclass.nome, userclass.id]}
  end

  def create
    @user = User.new(user_params)
    @userclass = UserClass.all.collect{|userclass|[userclass.nome, userclass.id]}
    @user.qnt_moedas = 15
    @user.level = 1

    if @user.save
      redirect_to user_path(@user)
    else
      render 'new'
    end
  end

  def update
    @user = User.find(params[:id])
    @userclass = UserClass.all.collect{|userclass|[userclass.nome, userclass.id]}
    if @user.update(user_params)
      redirect_to user_path(@user)
    else
      render 'edit'
    end
  end

  def edit
    @user = User.find(params[:id])
    @userclass = UserClass.all.collect{|user_class|[user_class.nome, user_class.id]}
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to action: "index"
  end
  
  private
  def user_params
    params.require(:user).permit(:nome, :apelido, :email, :data_nascimento, :local_nascimento, :genero, :user_class_id, :group_id, :password, :password_confirmation)
  end
end