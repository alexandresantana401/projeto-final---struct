class UserClassesController < ApplicationController
  def new
    @userclass = UserClass.new()
  end

  def create
    @userclass = UserClass.new(nome:params[:user_class][:nome])
    if @userclass.save
      redirect_to user_class_path(@userclass)
    else
      render 'new'
    end
  end

  def edit
    @userclass = UserClass.find(params[:id])
  end

  def update
    @userclass = UserClass.find(params[:id])
    if @userclass.update(nome:params[:user_class][:nome])
      redirect_to user_class_path(@userclass)
    else
      render 'edit'
    end
  end

  def new_subclass
    @subclass = UserSubclass.new()
  end

  def create_subclass
    @subclass = UserSubclass.new(user_class_id:params[:id], nome:params[:nome])
    if @subclass.save
      redirect_to user_class_path(@subclass.user_class)
    else
      render 'new_subclass'
    end
  end

  def edit_subclass
    @subclass = UserSubclass.find(params[:id])
  end

  def update_subclass
    @subclass = UserSubclass.find(params[:id])
    if @subclass.update(nome:params[:nome])
      redirect_to user_class_path(@subclass.user_class)
    else
      render 'edit_subclass'
    end
  end

  def index
    @userclass = UserClass.all
    @skill = Skill.all
  end

  def show
    @userclass = UserClass.find(params[:id])
  end

  private
  
  def class_params
    params.require(:user_class).permit(:nome)
  end

end

