class UserClass < ApplicationRecord
    has_many :user_subclasses
    has_many :users
    has_many :skills
end
