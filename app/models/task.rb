class Task < ApplicationRecord
    has_many :people_tasks
    has_many :users
    has_many :groups
    has_many :comments
end
