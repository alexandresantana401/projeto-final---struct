class User < ApplicationRecord
  authenticates_with_sorcery!
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }


  #belongs_to :member, optional: true
  
  has_one :member
  belongs_to :group, :optional => true
  belongs_to :user_class

  has_many :comments
  
  
  has_many :people_tasks
  belongs_to :tasks, :optional => true

  has_many :user_skills
  has_many :skills

end
