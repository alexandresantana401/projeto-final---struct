class UserSubclass < ApplicationRecord
  belongs_to :user_class
  has_many :skills
end
