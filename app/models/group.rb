class Group < ApplicationRecord
    has_many :members
    has_many :users

    belongs_to :people_tasks, :optional => true
end
