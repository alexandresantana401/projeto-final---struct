class PeopleTask < ApplicationRecord
  belongs_to :user, :optional => true
  belongs_to :group, :optional => true
  belongs_to :task
end
