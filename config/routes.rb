Rails.application.routes.draw do
  resources :groups
  get 'groups/:id/new_member/', to: 'groups#new_member', as: "new_member"
  post '/groups/:id/new_member/', to: 'groups#create_member', as: "create_member"
  post '/groups/destroy_member/:id', to: 'groups#destroy_member', as: "destroy_member"
  post '/groups/:id/update_member/', to:'groups#update_member', as: "update_member" 

  resources :user_classes, except:[:destroy]
  get '/user_classes/:id/new_subclass/', to: 'user_classes#new_subclass', as: "new_subclass"
  post '/user_classes/:id/new_subclass/', to: 'user_classes#create_subclass', as: "create_subclass"
  get '/user_classes/:id/edit_subclass/', to:'user_classes#edit_subclass', as: "edit_subclass"
  post '/user_classes/:id/edit_subclass/', to:'user_classes#update_subclass', as: "update_subclass"
  
  resources :sessions, only:[:new, :create, :destroy]
  get "/login", to: "sessions#new", as: "login"
  post "/logout", to: "sessions#destroy", as: "logout"
  root 'sessions#new'

  resources :users
  resources :comments
  get 'comments/:id/show_comment/', to: 'comments#show_comment', as: "show_comment"
  get 'comments/:id/edit_comment/', to:'comments#edit_comment', as: "edit"

  resources :skills
  
  resources :tasks
  get '/tasks/:id/partipar', to: 'tasks#assign_new', as: 'new_assign'
  get '/tasks/:id/comentarios', to: 'tasks#show_comments', as: 'show_comments'
  post '/tasks/:id/partipar', to: 'tasks#assign', as: 'create_assign'
  post '/tasks/:id/increase_xp', to: 'tasks#increase_xp', as: 'increase_xp'
  post '/tasks/:id/increase_time', to: 'tasks#increase_time', as: 'increase_time'
end
